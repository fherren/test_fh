﻿using System;

public class Class1
{
	public Class1()
	{
	}

    // 2.1 Kommentar für BB
    public virtual string GetSliderCode()
    {
        StringBuilder sb = new StringBuilder();
        List<SliderItem> SliderItems = new List<SliderItem>();
        string[] Zeilen = System.IO.File.ReadAllLines(@"C:\inetpub\wwwroot\AspDotNetStorefront\Web\Slider\Sliderdaten.txt");
        //Textfile auslesen
        foreach (string Zeile in Zeilen)
        {
            try
            {
                SliderItem item = new SliderItem();
                item.Bildname = Zeile.Split('#')[0];
                item.URL = Zeile.Split('#')[1];
                item.Text = Zeile.Split('#')[2];
                SliderItems.Add(item);
            }
            catch
            {

            }
        }

        string RandomImageLinkPath = "";

        StringBuilder SBLinkHref = new StringBuilder();
        StringBuilder SBLinkAltText = new StringBuilder();

        //Liste ausgeben und in String speichern
        foreach (SliderItem Item in SliderItems)
        {
            SBLinkHref.Append(Item.URL + ":");
            SBLinkAltText.Append(Item.Text + ":");
        }

        string[] arrLinkHref = SBLinkHref.ToString().Split(new char[] { ':' });
        string[] arrLinkAltText = SBLinkAltText.ToString().Split(new char[] { ':' });

        Numbers nbs = new Numbers();
        int Total = arrLinkHref.Length;
        ArrayList lstNumbers = nbs.RandomNumbers(Total);

        // Die zufällig generierten Numbern, welche im Array gespeichert wurde, werden nur den Links zugeteilt
        for (int a = 0; a < lstNumbers.Count; a++)
        {
            RandomImageLinkPath = String.Format("/Slider/images/image-slider-{0}.jpg", lstNumbers[a]);

            sb.Append("<A href=\"http://www.starworld.ch/" + arrLinkHref[Convert.ToInt32(lstNumbers[a]) - 1] + ".aspx\"><img src=\"" + RandomImageLinkPath + "\" alt=\"" + arrLinkAltText[Convert.ToInt32(lstNumbers[a]) - 1] + "\" />");
        }

        return sb.ToString();
    }

    public class SliderItem
    {
        public string Bildname;
        public string URL;
        public string Text;
    }

    class Numbers
    {
        public ArrayList RandomNumbers(int max)
        {
            // Create an ArrayList object that will hold the numbers
            ArrayList lstNumbers = new ArrayList();
            // The Random class will be used to generate numbers
            Random rndNumber = new Random();

            // Generate a random number between 1 and the Max
            int number = rndNumber.Next(1, max);
            // Add this first random number to the list
            lstNumbers.Add(number);
            // Set a count of numbers to 0 to start
            int count = 0;

            do // Repeatedly...
            {
                // ... generate a random number between 1 and the Max
                number = rndNumber.Next(1, max);

                // If the newly generated number in not yet in the list...
                if (!lstNumbers.Contains(number))
                {
                    // ... add it
                    lstNumbers.Add(number);
                }

                // Increase the count
                count++;
            } while (count <= 10 * max); // Do that again

            // Once the list is built, return it
            return lstNumbers;
        }
    }

    //===========================================================================================

    public virtual string GetSliderCodeKompakter()
    {
        StringBuilder sb = new StringBuilder();
        List<SliderItem> SliderItems = new List<SliderItem>();
        string[] Zeilen = System.IO.File.ReadAllLines(@"C:\inetpub\wwwroot\AspDotNetStorefront\Web\Slider\Sliderdaten.txt");
        //Textfile auslesen
        foreach (string Zeile in Zeilen)
        {
            try
            {
                SliderItem item = new SliderItem();
                item.Bildname = Zeile.Split('#')[0];
                item.URL = Zeile.Split('#')[1];
                item.Text = Zeile.Split('#')[2];
                SliderItems.Add(item);

            }
            catch
            {

            }
        }

        /*
        //Produziert fehler -
        Random rndNumber = new Random();
        int Verfuegbar = SliderItems.Count;
        int i = SliderItems.Count;
        for (int a = 0; a < i; a++)
        {
            // - indem durch das -1 am Ende immer die letzte Zahl - hier die 24 - steht
            int number = rndNumber.Next(0, Verfuegbar - 1);
            SliderItem Curitem = SliderItems[number];
            sb.Append("<A href=\"" + Curitem.URL + "\"><img src=\"/Slider/images/" + Curitem.Bildname + "\" alt=\"" + Curitem.Text + "\"/>");
            SliderItems.Remove(Curitem);
            Verfuegbar = Verfuegbar - 1;
        }
        return sb.ToString();
        */

        Random rndNumber = new Random();
        int Verfuegbar = SliderItems.Count;
        int si = SliderItems.Count;
        for (int a = 0; a < si; a++)
        {
            int number = rndNumber.Next(0, Verfuegbar);
            try
            {
                SliderItem Curitem = SliderItems[number];
                sb.Append("<A href=\"" + Curitem.URL + "\"><img src=\"/Slider/images/" + Curitem.Bildname + "\" alt=\"" + Curitem.Text + "\"/>");
                SliderItems.Remove(Curitem);
                Verfuegbar = Verfuegbar - 1;
            }
            catch
            {
            }
        }
        return sb.ToString();

        /*

        string RandomImageLinkPath = "";

        StringBuilder SBLinkHref = new StringBuilder();
        StringBuilder SBLinkAltText = new StringBuilder();

        //Liste ausgeben und in String speichern
        foreach (SliderItem Item in SliderItems)
        {
            SBLinkHref.Append(Item.URL + ":");
            SBLinkAltText.Append(Item.Text + ":");
        }


        string[] arrLinkHref = SBLinkHref.ToString().Split(new char[] { ':' });
        string[] arrLinkAltText = SBLinkAltText.ToString().Split(new char[] { ':' });
			
        Numbers nbs = new Numbers();
        int Total = arrLinkHref.Length;
        ArrayList lstNumbers = nbs.RandomNumbers(Total);

        // Die zufällig generierten Numbern, welche im Array gespeichert wurde, werden nur den Links zugeteilt
        for (int a = 0; a < lstNumbers.Count; a++)
        {
            RandomImageLinkPath = String.Format("/Slider/images/image-slider-{0}.jpg", lstNumbers[a]);

            sb.Append("<A href=\"http://www.starworld.ch/" + arrLinkHref[Convert.ToInt32(lstNumbers[a])-1] + ".aspx\"><img src=\"" + RandomImageLinkPath + "\" alt=\"" + arrLinkAltText[Convert.ToInt32(lstNumbers[a])-1] + "\" />");
        }
			
        return sb.ToString();
         */
    }

    public class SliderItem
    {
        public string Bildname;
        public string URL;
        public string Text;
    }

    class Numbers
    {
        public ArrayList RandomNumbers(int max)
        {
            // Create an ArrayList object that will hold the numbers
            ArrayList lstNumbers = new ArrayList();
            // The Random class will be used to generate numbers
            Random rndNumber = new Random();

            // Generate a random number between 1 and the Max
            int number = rndNumber.Next(1, max);
            // Add this first random number to the list
            lstNumbers.Add(number);
            // Set a count of numbers to 0 to start
            int count = 0;

            do // Repeatedly...
            {
                // ... generate a random number between 1 and the Max
                number = rndNumber.Next(1, max);

                // If the newly generated number in not yet in the list...
                if (!lstNumbers.Contains(number))
                {
                    // ... add it
                    lstNumbers.Add(number);
                }

                // Increase the count
                count++;
            } while (count <= 10 * max); // Do that again

            // Once the list is built, return it
            return lstNumbers;
        }
    }

}



